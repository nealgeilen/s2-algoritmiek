﻿using System;
using Xunit;
using FluentAssertions;
using Circustrein;
using System.Collections.Generic;

namespace CircustreinTest
{
    public class ImplementationTest
    {
        [Fact]
        public void H5Small_H3Medium_H1Big()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(2);
        }

        [Fact]
        public void H3Big_C1Small_C3Medium_C1Big()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(6);
        }

        [Fact]
        public void H5Small_H5Medium_H5Big_C2Small_C2Medium_C2Big()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C2", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C2", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(8);
        }

        [Fact]
        public void H5Small_H5Medium_H5Big_C2Small_C2Big_C2Medium()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(8);
        }


        [Fact]
        public void H1Small_H3Medium_H2Big()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(2);
        }

        [Fact]
        public void H3Medium_H2Big_C1Small()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
            };

            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(2);
        }
    }
}
