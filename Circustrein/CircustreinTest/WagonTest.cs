﻿using System;
using Xunit;
using FluentAssertions;
using Circustrein;
using System.Collections.Generic;

namespace CircustreinTest
{
    public class WagonTest
    {
        [Fact]
        public void Constructor()
        {
            var animal = new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor);

            animal.Weight.Should().Be(Animal.Sizes.SMALL);
            animal.Name.Should().Be("C1");
            animal.IsACarnivor().Should().Be(true);
        }


        [Fact]
        public void IsWagonFull()
        {
            Circustrein.Wagon wagon = new Circustrein.Wagon(new List<Animal>() {
                new Animal("Meerkat", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.BIG, Animal.FoodType.Herbivore)
            });

            bool result = wagon.CanAnimalBeAdded(new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore));

            result.Should().Be(false);
        }


        [Fact]
        public void CanAnimaleBePlacedWithAMeatLover()
        {
            Circustrein.Wagon wagon = new Circustrein.Wagon(new List<Animal>() {
                new Animal("Meerkat", Animal.Sizes.BIG, Animal.FoodType.Carnivor)
            });

            bool result = wagon.CanAnimalBeAdded(new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore));

            result.Should().Be(false);
        }

        [Fact]
        public void IsTherePlaceForCharnivor()
        {
            Circustrein.Wagon wagon = new Circustrein.Wagon(new List<Animal>() {
                new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor)
            });
  

            bool result = wagon.CanAnimalBeAdded(new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor));

            result.Should().Be(false);
        }

        [Fact]
        public void WeightWagon()
        {
            Circustrein.Wagon wagon = new Circustrein.Wagon(new List<Animal>() {
                new Animal("Meerkat", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore)

            });

            wagon.GetWeight().Should().Be(10);
        }
        [Fact]
        public void GetMeatLover()
        {
            Circustrein.Wagon wagon = new Circustrein.Wagon(new List<Animal>() {
                new Animal("Meerkat", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("Meerkat", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore)
            });
            Animal MeatLover = new Animal("Meerkat", Animal.Sizes.SMALL, Animal.FoodType.Carnivor);

            wagon.AddAnimal(MeatLover);

            wagon.GetAnimalWhoLovesMeat().Should().Be(MeatLover);
        }
    }
}
