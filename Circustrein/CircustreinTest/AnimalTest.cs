﻿using System;
using Xunit;
using FluentAssertions;
using Circustrein;

namespace CircustreinTest
{
    public class AnimalTest
    {
        [Fact]
        public void Constructor()
        {
            var animal = new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor);

            animal.Weight.Should().Be(Animal.Sizes.SMALL);
            animal.Name.Should().Be("C1");
            animal.IsACarnivor().Should().Be(true);
        }
    }
}
