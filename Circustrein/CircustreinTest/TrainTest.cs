﻿using System;
using Xunit;
using FluentAssertions;
using Circustrein;
using System.Collections.Generic;

namespace CircustreinTest
{
    public class TrainTest
    {

        [Fact]
        public void MainFunction()
        {
            List<Animal> list = new List<Animal> {
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H1", Animal.Sizes.SMALL, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H3", Animal.Sizes.MEDIUM, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
            };


            Train train = Train.FillTrain(list);

            train.Wagons.Count.Should().Be(8);
        }
    }
}
