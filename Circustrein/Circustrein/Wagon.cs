﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Circustrein
{
    public class Wagon
    {
        public List<Animal> Animals { get; set; } = new List<Animal> { };

        public Wagon(List<Animal> animals = null)
        {
            this.Animals = animals;
        }


        public bool CanAnimalBeAdded(Animal animal)
        { 
            if (animal.isCarnivorSuitableForAWagon(this))
            {
                return false;
            }

            if (animal.CanAnimaleBePlacedWithAMeatLover(this) == false)
            {
                return false;
            }

            if (this.IsWagonWeightMax(animal))
            {
                return false;
            }

           return true;
        }

        protected bool IsWagonWeightMax(Animal animal)
        {
            int wagonWeight = this.GetWeight();
            if (wagonWeight >= 10 || wagonWeight + (int)animal.Weight > 10)
            {
                return true;
            }
            return false;
        }

        override public string ToString()
        {
            string names = "";
            foreach (Animal animal in this.Animals)
            {
                names += animal.ToString() + ",";
            }
            return names;
        }

        public int GetWeight()
        {
            int weight = 0;
            foreach (Animal animal in this.Animals)
            {
                weight += (int)animal.Weight;
            }
            return weight;
        }


        public Animal GetAnimalWhoLovesMeat()
        {
            foreach (Animal animal in this.Animals)
            {
                if (animal.IsACarnivor())
                {
                    return animal;
                }
            }
            return null;
        }

        public void AddAnimal(Animal animal)
        {
            this.Animals.Add(animal);
            Console.WriteLine("An empty spot has bin found and a " + animal.Name + " is placed");
        }
    }
}
