﻿using System;
using System.Collections.Generic;

namespace Circustrein
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Animal> list = new List<Animal> {
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("H5", Animal.Sizes.BIG, Animal.FoodType.Herbivore),
                new Animal("C1", Animal.Sizes.SMALL, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C3", Animal.Sizes.MEDIUM, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
                new Animal("C5", Animal.Sizes.BIG, Animal.FoodType.Carnivor),
            };


            Train train = Train.FillTrain(list);

            foreach(Wagon wagon in train.Wagons)
            {
                Console.WriteLine(wagon);
            }

            Console.WriteLine(train.Wagons.Count);
        }
    }
}
