﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Circustrein
{

    public class Animal
    {
        private Sizes _weight;
        public Sizes Weight { get => _weight; protected set => _weight = value; }

        private string _name;
        public string Name { get => _name; protected set => _name = value; }


        private FoodType _foodType;
        public FoodType Food { get => _foodType; protected set => _foodType = value; }

        public enum Sizes {
            SMALL = 1,
            MEDIUM = 3,
            BIG = 5
        }

        public bool IsACarnivor()
        {
            return this.Food == FoodType.Carnivor;
        }

        public bool IsAHerbivore()
        {
            return this.Food == FoodType.Herbivore;
        }


        public enum FoodType{
            Carnivor = 1,
            Herbivore = 0

        }

        public Animal(string name, Sizes weight, FoodType foodType)
        {
            this.Name = name;
            this.Weight = weight;
            this.Food = foodType;
        }


        public bool CanAnimaleBePlacedWithAMeatLover(Wagon wagon)
        {
            if (wagon.GetAnimalWhoLovesMeat() != null)
            {
                if (wagon.GetAnimalWhoLovesMeat().Weight >= this.Weight)
                {
                    return false;
                }
            }
            return true;
        }


        public bool isCarnivorSuitableForAWagon(Wagon wagon)
        {
            if (this.IsACarnivor())
            {
                if (wagon.GetAnimalWhoLovesMeat() != null)
                {
                    return false;
                }
                foreach (Animal animalFromWagon in wagon.Animals)
                {
                    if (animalFromWagon.Weight <= this.Weight)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }


        override public string ToString()
        {
            return this.Name + " " + this.Weight + " " + this.Food.ToString();
        }
    }
}
