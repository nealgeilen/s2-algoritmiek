﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Circustrein
{
    public class Train
    {
        private List<Wagon> _wagons = new List<Wagon> { };
        public List<Wagon> Wagons { get => _wagons; protected set => _wagons = value; }


        public static Train FillTrain(List<Animal> animals)
        {
            Train train = new Train();

            List<Animal> orderdAnimals =  train.OrderList(animals);

            train.AddAnimals(orderdAnimals);

            return train;
        }

        public List<Animal> OrderList(List<Animal> animals)
        {
            List<Animal> animalWeightOrdered = animals.OrderByDescending(animal => animal.Weight)
                .ToList();

            List<Animal> meatLovers = animalWeightOrdered.Where(animal => animal.IsACarnivor())
                .ToList();

            List<Animal> smallMediumHerbivors = animalWeightOrdered
                .Where(animal => animal.IsAHerbivore() && (animal.Weight == Animal.Sizes.MEDIUM || animal.Weight == Animal.Sizes.SMALL))
                .ToList();
            List<Animal> bigHerbivors = animalWeightOrdered
                .Where(animal => animal.IsAHerbivore() && animal.Weight == Animal.Sizes.BIG)
                .ToList();

            List<Animal> OrderdList = new List<Animal>();

            OrderdList.AddRange(meatLovers);
            OrderdList.AddRange(smallMediumHerbivors);
            OrderdList.AddRange(bigHerbivors);

            return OrderdList;
        }

        protected void AddAnimal(Animal animal)
        {
            if (this.CanAdnimalBeAddedToWagon(animal))
            {
                this.AddToWagon(animal);
            } else
            {
                this.AddAnimalToNewWagon(animal);
            }
        }

        public void AddAnimals(List<Animal> AnimalsList)
        {
            foreach (Animal animal in AnimalsList)
            {
                this.AddAnimal(animal);
            }
        }

        protected bool CanAdnimalBeAddedToWagon(Animal animal)
        {
            foreach (Wagon wagon in this.Wagons)
            {
                if (wagon.CanAnimalBeAdded(animal))
                {
                    return true;
                }
            }
            return false;
        }

        protected bool AddToWagon(Animal animal)
        {
            foreach (Wagon wagon in this.Wagons)
            {
                if (wagon.CanAnimalBeAdded(animal))
                {
                    wagon.AddAnimal(animal);
                    return true;
                } 
            }
            return false;
        }

        protected void AddAnimalToNewWagon(Animal animal)
        {
            this.Wagons.Add(new Wagon(new List<Animal>() { animal }));
        }
    }
}
